<?php
namespace app\controllers;

use app\models\ServiceSettingsManager;
use yii\web\Controller;
use Yii;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $servicesSettings = [];

        // Заполняем массив с настройками для каждого сервиса
        foreach (Yii::$app->params['services'] as $serviceId => $serviceParams) {
            $service = $this->getServiceInstance($serviceParams);

            $servicesSettings[ $serviceId ] = $service->getSettings();
        }

        // Обновляем настройки для переданного сервиса
        if (Yii::$app->request->isPost()) {
            $serviceId = Yii::$app->request->post('serviceId');
            $serviceSettings = Yii::$app->request->post('serviceSettings');

            $service = $this->getServiceInstance(Yii::$app->params['services'][ $serviceId ]);

            $service->updateSettings($serviceSettings);
        }

        return $this->render('index', compact('servicesSettings'));
    }

    private function getServiceInstance($params): ServiceSettingsManager
    {
        $serviceSettingsClass = $params['class'];

        unset($params['class']);

        return new $serviceSettingsClass($params);
    }
}
