<?php
namespace app\models\ServiceThree;


class Connector implements \app\models\ServiceConnector
{
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getSettings(): array
    {
        // todo получение списка настроек для сервиса 3
    }

    public function updateSettings(array $settings): bool
    {
        // todo запись настроек для сервиса 3
    }
}