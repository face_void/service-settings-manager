<?php
namespace app\models\ServiceThree;


class SettingsManager extends \app\models\ServiceSettingsManager
{
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getServiceConnector(): \app\models\ServiceConnector
    {
        return new Connector($this->params);
    }
}