<?php
namespace app\models\ServiceOne;


class SettingsManager extends \app\models\ServiceSettingsManager
{
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getServiceConnector(): \app\models\ServiceConnector
    {
        return new Connector($this->params);
    }
}