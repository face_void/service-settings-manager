<?php
namespace app\models\ServiceOne;


class Connector implements \app\models\ServiceConnector
{
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getSettings(): array
    {
        // todo получение списка настроек для сервиса 1
    }

    public function updateSettings(array $settings): bool
    {
        // todo запись настроек для сервиса 1
    }
}