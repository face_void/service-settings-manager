<?php
namespace app\models;

/**
 * Фабрика для работы с сервисами
 */
abstract class ServiceSettingsManager
{
    protected $service;

    /**
     * Получает коннектор конкретного сервиса
     *
     * @return ServiceConnector
     */
    abstract public function getServiceConnector(): ServiceConnector;

    /**
     * Возвращает коннектор сервиса, с которым сейчас ведётся работа
     *
     * @return ServiceConnector
     */
    private function getService(): ServiceConnector
    {
        if (!$this->service) {
            $this->service = $this->getServiceConnector();
        }

        return $this->service;
    }

    /**
     * Возвращает список настроек сервиса
     *
     * @return array
     */
    public function getSettings()
    {
        return $this->getService()->getSettings();
    }

    /**
     * Записывает настройки сервиса
     *
     * @param array $settings
     * @return bool
     */
    public function updateSettings(array $settings)
    {
        return $this->getService()->updateSettings($settings);
    }
}