<?php

namespace app\models;

interface ServiceConnector
{
    public function getSettings(): array;

    public function updateSettings(array $settings): bool;
}