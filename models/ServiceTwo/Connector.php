<?php
namespace app\models\ServiceTwo;


class Connector implements \app\models\ServiceConnector
{
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getSettings(): array
    {
        // todo получение списка настроек для сервиса 2
    }

    public function updateSettings(array $settings): bool
    {
        // todo запись настроек для сервиса 2
    }
}