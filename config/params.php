<?php

return [
    'services' => [
         'service-one' => [
            'class' => "app\models\ServiceOne\SettingsManager",
            'pathToSettings' => 'fixtures/service-one/settings.json',
        ],
         'service-two' => [
            'class' => "app\models\ServiceTwo\SettingsManager",
            'pathToSettings' => 'fixtures/service-two/settings.yaml',
        ],
         'service-three' => [
            'class' => "app\models\ServiceThree\SettingsManager",
            'pathToSettings' => 'fixtures/service-three/settings.txt',
        ],
    ],
];
