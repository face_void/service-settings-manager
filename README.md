# Сервис для получения и редактирования настроек других сервисов
## Установка
```bash
git clone https://gitlab.com/face_void/service-settings-manager.git
```
```bash
composer install
```
## Настройка
Настройки сервисов хранятся в config/params.php. В параметре 'class' задаётся полный путь к классу, который отвечает за работу с конкретным сервисом.
```php
return [
    'services' => [
         'service-id' => [
            'class' => "namespace\\to\\class\\SettingsManager",
            // Настройки для сервиса (путь к файлу с конфигом, параметры авторизации и т.д.)
        ],
        ...
    ],
];
```